<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="<?php print $language ?>" xml:lang="<?php print $language ?>">
<head>
<title><?php print $head_title ?></title>
<meta http-equiv="Content-Style-Type" content="text/css" />
<?php print $head ?><?php print $styles ?>
</head>

<body id="home">
	<div id="header">
		<div class="slogan"><?php print($site_slogan) ?></div> 
		<?php if (is_array($primary_links)) : ?> 
			<div id="navigation"><h3 class="hidden">Navigation</h3>
			<ul> 
			<?php foreach ($primary_links as $link): ?> 
			<li><?php print $link?></li> 
			<?php endforeach; ?> 
			</ul></div>
		<?php endif; ?>
	</div>


<div id="wrapper">
	<div id="container">
		<div id="content">
			<div class="contentdiv">
				<h2><?php print($site_name) ?></h2>
				<div class="divweblog">
				<!--Database Generated Content-->
					<div class="navigation"> <?php print $breadcrumb ?> </div> 
					<?php if ($messages != ""): ?> 
					<div id="message"><?php print $messages ?></div> 
					<?php endif; ?> 
					<?php if ($mission != ""): ?> 
					<div id="mission"><?php print $mission ?></div> 
					<?php endif; ?> 
					<?php if ($title != ""): ?> 
					<h2 class="page-title"><?php print $title ?></h2> 
					<?php endif; ?> 
					<?php if ($tabs != ""): ?> 
					<?php print $tabs ?> 
					<?php endif; ?> 
					<?php if ($help != ""): ?> 
					<p id="help"><?php print $help ?></p> 
					<?php endif; ?> 
					<!-- start main content --> 
					<?php print($content) ?> 
					<!-- end main content --> 
				<!--End Database Generated Content-->
	      	</div>
    		</div>
  		</div>
	</div>

<div id="sidebar">
	<?php if ($search_box): ?> 
		<form action="<?php print $search_url ?>" method="post" id="searchform"> 
		<input class="form-text" type="text" size="15" value="" name="edit[keys]" id="s" /> 
		<input class="form-submit" type="submit" value="<?php print $search_button_text ?>" id="searchsubmit" /> 
		</form> 
	<?php endif; ?> 
	<?php print $sidebar_left ?>
	<?php print $sidebar_right ?>
</div>


<div class="clearing">&nbsp;</div>

<!--Close Wrapper-->
</div>

<div id="footer">
	<div id="credits">Designed by <a href="http://www.afterdeathgraphics.com">AfterDeath Graphics </a> <br />Powered by <a href="http://www.drupal.org">Drupal</a></div>
</div>
<?php print $closure;?> 
</body>
</html>